using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Collections;
using Unity.Jobs;
// ReSharper disable UseObjectOrCollectionInitializer
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable FieldCanBeMadeReadOnly.Global
// ReSharper disable InconsistentNaming
// Important: We keep variables internal, use "m_" naming convention, and don't change the declaration order, for the variables that are required by the Job system
namespace DotsPractice.Collections
{
	[StructLayout(LayoutKind.Sequential)]
	[NativeContainer]
	[NativeContainerSupportsMinMaxWriteRestriction]
	[NativeContainerSupportsDeallocateOnJobCompletion]
	public unsafe struct NativeCustomArray<T> : INativeDisposable where T : unmanaged
	{
		[NativeDisableUnsafePtrRestriction]
		internal void* m_Buffer;
		internal int m_Length;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
		internal int m_MinIndex;
		internal int m_MaxIndex;
		internal AtomicSafetyHandle m_Safety;

		[NativeSetClassTypeToNullOnSchedule]
		internal DisposeSentinel m_DisposeSentinel;

		// I don't know why we use SharedStatic<int> instead of int, code copied from NativeList.cs
		static readonly SharedStatic<int> s_staticSafetyId = SharedStatic<int>.GetOrCreate<NativeCustomArray<T>>();
#endif

		internal Allocator m_AllocatorLabel;
		public int Length => m_Length;
		
		public bool IsCreated => m_Buffer != null;

		public NativeCustomArray(int length, Allocator allocator,
		                         NativeArrayOptions options = NativeArrayOptions.ClearMemory)
		{
			Allocate(length, allocator, out this);

			if(options == NativeArrayOptions.ClearMemory)
				UnsafeUtility.MemClear(m_Buffer, Length * (long)UnsafeUtility.SizeOf<T>());
		}

		static void Allocate(int length, Allocator allocator, out NativeCustomArray<T> array)
		{
			var totalSize = UnsafeUtility.SizeOf<T>() * (long)length;
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckAllocateArguments(length, allocator, totalSize);
#endif
			array = new NativeCustomArray<T>();
			array.m_Buffer = UnsafeUtility.Malloc(totalSize, UnsafeUtility.AlignOf<T>(), allocator);
			array.m_Length = length;
			array.m_AllocatorLabel = allocator;
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			array.m_MinIndex = 0;
			array.m_MaxIndex = length - 1;
			DisposeSentinel.Create(out array.m_Safety, out array.m_DisposeSentinel, 1, allocator);
			InitStaticSafetyId(ref array.m_Safety);
#endif
		}

		[BurstDiscard]
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void InitStaticSafetyId(ref AtomicSafetyHandle safetyHandle)
		{
			if(s_staticSafetyId.Data == 0)
				s_staticSafetyId.Data = AtomicSafetyHandle.NewStaticSafetyId<NativeCustomArray<T>>();

			AtomicSafetyHandle.SetStaticSafetyId(ref safetyHandle, s_staticSafetyId.Data);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckAllocateArguments(int length, Allocator allocator, long totalSize)
		{
			CheckAllocator(allocator);
			CheckLength(length);
			CheckTotalSize(nameof(length), totalSize);
			CheckIsBlittable();
			CheckIsValidElementType();
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIsValidElementType()
		{
			if(!UnsafeUtility.IsValidNativeContainerElementType<T>())
				throw new InvalidOperationException(
					$"{typeof(T)} used in NativeCustomArray<{typeof(T)}> must be unmanaged (contain no managed types) and cannot itself be a native container type.");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIsBlittable()
		{
			if(!UnsafeUtility.IsBlittable<T>())
				throw new ArgumentException(string.Format("{0} used in NativeCustomArray<{0}> must be blittable",
					typeof(T)));
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckAllocator(Allocator allocator)
		{
			// Native allocation is only valid for Temp, Job and Persistent.
			if(allocator <= Allocator.None)
				throw new ArgumentException("Allocator must be Temp, TempJob or Persistent", nameof(allocator));
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckLength(int length)
		{
			if(length < 0)
				throw new ArgumentOutOfRangeException(nameof(length), "Length must be >= 0");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckTotalSize(string lengthParamName, long totalSize)
		{
			// Make sure we cannot allocate more than int.MaxValue (2,147,483,647 bytes)
			// because the underlying UnsafeUtility.Malloc is expecting a int.
			if(totalSize > int.MaxValue)
				throw new ArgumentOutOfRangeException(lengthParamName,
					$"Length * sizeof(T) cannot exceed {int.MaxValue} bytes");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckElementReadAccess(int index)
		{
			AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
			CheckIndexInRange(index);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckElementWriteAccess(int index)
		{
			AtomicSafetyHandle.CheckWriteAndThrow(m_Safety);
			CheckIndexInRange(index);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckIndexInRange(int index)
		{
			if(index < m_MinIndex || index > m_MaxIndex)
				FailOutOfRangeError(index);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void FailOutOfRangeError(int index)
		{
			if(index < Length && (m_MinIndex != 0 || m_MaxIndex != Length - 1))
				throw new IndexOutOfRangeException(
					$"Index {index} is out of restricted IJobParallelFor range [{m_MinIndex}...{m_MaxIndex}] in ReadWriteBuffer.\n" +
					"ReadWriteBuffers are restricted to only read & write the element at the job index. " +
					"You can use double buffering strategies to avoid race conditions due to " +
					"reading & writing in parallel to the same elements from a job.");

			throw new IndexOutOfRangeException($"Index {index} is out of range of '{Length}' Length.");
		}

		public T this[int index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckElementReadAccess(index);
#endif
				return UnsafeUtility.ReadArrayElement<T>(m_Buffer, index);
			}

			[WriteAccessRequired]
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckElementWriteAccess(index);
#endif
				UnsafeUtility.WriteArrayElement(m_Buffer, index, value);
			}
		}

		public T[] ToArray()
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif

			var array = new T[Length];
			for(var i = 0; i < Length; i++)
				array[i] = UnsafeUtility.ReadArrayElement<T>(m_Buffer, i);
			return array;
		}

		void CheckIsNullPtrAndThrow()
		{
			if(m_Buffer == null)
				throw new ObjectDisposedException("The NativeCustomArray is already disposed.");
		}

		void CheckInvalidAllocatorAndThrow()
		{
			if(m_AllocatorLabel == Allocator.Invalid)
				throw new InvalidOperationException(
					"The NativeCustomArray can not be Disposed because it was not allocated with a valid allocator.");
		}
		
		public void Dispose()
		{
			CheckIsNullPtrAndThrow();
			CheckInvalidAllocatorAndThrow();

			if(m_AllocatorLabel > Allocator.None)
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
				UnsafeUtility.Free(m_Buffer, m_AllocatorLabel);
				m_AllocatorLabel = Allocator.Invalid;
			}

			m_Buffer = (void*)IntPtr.Zero;
			m_Length = 0;
		}

		public JobHandle Dispose(JobHandle inputDeps)
		{
			CheckIsNullPtrAndThrow();
			CheckInvalidAllocatorAndThrow();
			
			if(m_AllocatorLabel == Allocator.None)
			{
				m_Buffer = (void*)IntPtr.Zero;
				m_Length = 0;
				return inputDeps;
			}
			
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Clear(ref m_DisposeSentinel);
			
			var jobHandle = new NativeCustomArrayDisposeJob
			{
				Data = new NativeCustomArrayDispose
				{
					Allocator = m_AllocatorLabel,
					Buffer = m_Buffer,
				}
			}.Schedule(inputDeps);
			
			AtomicSafetyHandle.Release(m_Safety);
#else
			var jobHandle = new NativeCustomArrayDisposeJob
			{
				Data = new NativeCustomArrayDispose
				{
					Allocator = m_AllocatorLabel,
					Buffer = m_Buffer,
				}
			}.Schedule(inputDeps);
#endif
			m_Buffer = (void*)IntPtr.Zero;
			m_Length = 0;
			m_AllocatorLabel = Allocator.Invalid;
			return jobHandle;
		}
	}
}