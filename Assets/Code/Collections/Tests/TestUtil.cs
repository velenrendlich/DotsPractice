using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.TestTools;

namespace DotsLibrary.Collections.Tests
{
	public static class TestUtil
	{
		public static void ExpectInvalidOperationException()
		{
			LogAssert.Expect(LogType.Exception, new Regex("System::InvalidOperationException*"));
		}
	}
}