using System.Diagnostics;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using Debug = UnityEngine.Debug;
using Random = Unity.Mathematics.Random;

namespace DotsLibrary.SpacePartitioning
{
	public partial class CreatePartitionUnitsSystem : SystemBase
	{
		protected override void OnCreate()
		{
			RequireForUpdate<CreatePartitionUnitsInit>();
		}

		protected override void OnUpdate()
		{
			Debug.Log("CreatePartitionUnitsSystem run.");

			var data = GetSingleton<CreatePartitionUnitsInit>();
			var random = new Random((uint)Stopwatch.GetTimestamp());
			EntityManager.Instantiate(data.Prefab, data.SpawnCount, Allocator.Temp).Dispose();
			var spawnRadius = data.SpawnRadius;

			Entities.WithAll<SpacePartitionUnit>()
			        .ForEach((ref Translation translation) =>
			        {
				        translation.Value = random.NextFloat3() * spawnRadius;
			        })
			        .Run();

			EntityManager.DestroyEntity(GetSingletonEntity<CreatePartitionUnitsInit>());
		}
	}
}