using Unity.Entities;

namespace DotsLibrary.SpacePartitioning
{
	[GenerateAuthoringComponent]
	public struct CreatePartitionUnitsInit : IComponentData
	{
		public Entity Prefab;
		public int SpawnCount;
		public float SpawnRadius;
	}
}