using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLibrary.SpacePartitioning
{
	public partial class KeepInPermittedBoundsSystem : SystemBase
	{
		protected override void OnCreate()
		{
			RequireForUpdate<PermittedBounds>();
		}

		protected override void OnUpdate()
		{
			var permittedBounds = GetSingleton<PermittedBounds>();
			var deltaTime = Time.DeltaTime;

			Entities.ForEach((ref Velocity velocity, ref Translation translation) =>
			        {
				        if(!IsInBounds(in permittedBounds, translation.Value))
				        {
					        // Reverse the Velocity and Move one in direction of new velocity
					        velocity.Value = -velocity.Value;
					        translation.Value += velocity.Value * deltaTime;
				        }
			        })
			        .ScheduleParallel();
		}

		static bool IsInBounds(in PermittedBounds bounds, float3 position)
		{
			return
				position.x <= bounds.Max.x &&
				position.y <= bounds.Max.y &&
				position.z <= bounds.Max.z &&
				position.x >= bounds.Min.x &&
				position.y >= bounds.Min.y &&
				position.z >= bounds.Min.z;
		}
	}
}