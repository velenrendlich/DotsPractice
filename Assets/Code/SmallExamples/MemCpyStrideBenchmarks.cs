using System;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.PerformanceTesting;

namespace DotsLibrary
{
	public static unsafe class MemCpyStrideBenchmarks
	{
		public struct StructA
		{
			public int ValueA;
		}

		public struct StructB
		{
			public int source;
			public int ValueB;
		}

		public const int Count = 100000;
		const int TargetValue = 10;
		public static readonly StructA[] Destination = new StructA[Count];
		public static readonly StructB[] Source = new StructB[Count];

		static readonly int SourceStride = sizeof(StructB);
		static readonly int DestinationStride = sizeof(StructA);
		static readonly int ElementSize = sizeof(int);

		[SetUp]
		public static void SetUp()
		{
			Array.Clear(Destination, 0, Count);

			for(int i = 0; i < Count; i++)
			{
				Source[i] = new StructB { source = TargetValue, ValueB = 0 };
			}
		}

		static void DoCopy()
		{
			for(int i = 0; i < Count; i++)
			{
				Destination[i].ValueA = Source[i].source;
			}
		}

		static void DoCopyBurstImpl([NoAlias] StructA* destination, [NoAlias] StructB* arrayB)
		{
			for(int i = 0; i < Count; i++)
			{
				destination[i].ValueA = arrayB[i].source;
			}
		}

		static void DoCopyStride()
		{
			fixed(StructB* source = Source)
			fixed(StructA* destination = Destination)
			{
				UnsafeUtility.MemCpyStride(destination, DestinationStride, source, SourceStride, ElementSize, Count);
			}
		}

		static void DoCopyBurst()
		{
			fixed(StructB* source = Source)
			fixed(StructA* destination = Destination)
			{
				DoCopyBurstImpl(destination, source);
			}
		}

		static bool CheckEquals()
		{
			for(int i = 0; i < Count; i++)
			{
				if(Destination[i].ValueA != Source[i].source)
					return false;
			}

			return true;
		}

		[Test]
		public static void DoesNotWorkByDefault()
		{
			Assert.IsFalse(CheckEquals());
		}

		[Test]
		public static void CopyForWorks()
		{
			DoCopy();
			Assert.IsTrue(CheckEquals());
		}

		[Test]
		public static void CopyBurstWorks()
		{
			DoCopyBurst();
			Assert.IsTrue(CheckEquals());
		}

		[Test]
		public static void CopyStrideWorks()
		{
			DoCopyStride();
			Assert.IsTrue(CheckEquals());
		}

		[Test, Performance]
		public static void CopyForBenchmark()
		{
			Measure.Method(DoCopy).IterationsPerMeasurement(10).MeasurementCount(10).WarmupCount(10).Run();
		}

		[Test, Performance]
		public static void CopyBurstBenchmark()
		{
			Measure.Method(DoCopyBurst).IterationsPerMeasurement(10).MeasurementCount(10).WarmupCount(10).Run();
		}

		[Test, Performance]
		public static void CopyStrideBenchmark()
		{
			Measure.Method(DoCopyStride).IterationsPerMeasurement(10).MeasurementCount(10).WarmupCount(10).Run();
		}
	}
}