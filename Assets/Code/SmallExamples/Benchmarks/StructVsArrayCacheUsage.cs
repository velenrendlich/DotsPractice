using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using Random = Unity.Mathematics.Random;

namespace DotsLibrary
{
	public static class StructVsArrayCacheUsage
	{
		public class DataA
		{
			public float4 Value;
		}

		public struct DataB
		{
			public float4 Value;
		}

		public class DummyObject
		{
			public float4x4 Data;
		}
		
		[Test, Performance]
		public static void SetClassBenchmark()
		{
			var classArray = new DataA[20000];
			var dummyObjects = new List<DummyObject>();
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       for(int i = 0; i < classArray.Length; i++)
				       {
					       classArray[i] = new DataA { Value = random.NextFloat4() };
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < classArray.Length; i++)
				       {
					       classArray[i] = new DataA { Value = random.NextFloat4() };
					      
					       for(int x = 0; x < 10; x++)
					       {
						       dummyObjects.Add(new DummyObject());
					       }
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void IterateClassBenchmark()
		{
			var classArray = new DataA[20000];
			var dummyObjects = new List<DummyObject>();
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       float4 sum = 0;

				       for(int i = 0; i < classArray.Length; i++)
				       {
					       sum += classArray[i].Value;
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < classArray.Length; i++)
				       {
					       classArray[i] = new DataA { Value = random.NextFloat4() };
					      
					       for(int x = 0; x < 10; x++)
					       {
						       dummyObjects.Add(new DummyObject());
					       }
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void IterateStructBenchmark()
		{
			var structArray = new DataB[20000];
			var dummyObjects = new List<DummyObject>();
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       float4 sum = 0;

				       for(int i = 0; i < structArray.Length; i++)
				       {
					       sum += structArray[i].Value;
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < structArray.Length; i++)
				       {
					       structArray[i] = new DataB { Value = random.NextFloat4() };

					       for(int x = 0; x < 10; x++)
					       {
						       dummyObjects.Add(new DummyObject());
					       }
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void SetStructBenchmark()
		{
			var structArray = new DataB[20000];
			var dummyObjects = new List<DummyObject>();
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       for(int i = 0; i < structArray.Length; i++)
				       {
					       structArray[i] = new DataB { Value = random.NextFloat4() };
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < structArray.Length; i++)
				       {
					       structArray[i] = new DataB { Value = random.NextFloat4() };

					       for(int x = 0; x < 10; x++)
					       {
						       dummyObjects.Add(new DummyObject());
					       }
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}


		[Test, Performance]
		public static void IterateStructBurstBenchmark()
		{
			
		}
	}
}