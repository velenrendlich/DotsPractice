using NUnit.Framework;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace DotsLibrary
{
	// TODO: Simplify this code example, you remember what to do.
	public static class UselessDataEffectOnCache
	{
		public struct EntityData
		{
			public float UsefulData1;
			public float4x4 UselessData;
			public float UsefulData2;
		}

		public struct UsefulDataAArray
		{
			public float[] Values;
		}

		public struct UsefulDataBArray
		{
			public float[] Values;
		}

		[Test, Performance]
		public static void AoSBenchmark()
		{
			var data = new EntityData[100000];
			
			Measure.Method(() =>
			       {
				       for(int i = 0; i < 100000; i++)
				       {
					       data[i].UsefulData1 += data[i].UsefulData2;
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void SoABenchmark()
		{
			var usefulDataA = new UsefulDataAArray { Values = new float[100000] };
			var usefulDataB = new UsefulDataBArray { Values = new float[100000] };

			Measure.Method(() =>
			       {
				       for(int i = 0; i < 100000; i++)
				       {
					       usefulDataA.Values[i] += usefulDataB.Values[i];
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
	}
}