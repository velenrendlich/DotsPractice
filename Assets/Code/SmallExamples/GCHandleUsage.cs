using System.Runtime.InteropServices;
using NUnit.Framework;
using Unity.Burst;

namespace DotsLibrary
{
	[BurstCompile]
	public static unsafe class GCHandleUsage
	{
		[Test]
		public static void PinIncrementWorks()
		{
			var count = 10000;
			var array = new int[count];
			var gcHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			var pointer = (int*)gcHandle.AddrOfPinnedObject();
			IncrementBurst(pointer, count);
			gcHandle.Free();

			for(int i = 0; i < count; i++)
			{
				Assert.AreEqual(1, array[i]);
			}
		}

		[BurstCompile]
		static void IncrementBurst(int* ptr, int count)
		{
			for(int i = 0; i < count; i++)
			{
				ptr[i]++;
			}
		}
	}
}