using System;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;

namespace DotsLibrary
{
	public static unsafe class BurstSpan
	{
		[BurstCompile]
		public struct SpanJob : IJob
		{
			public NativeArray<int> Array;
			public NativeReference<int> Result;

			public void Execute()
			{
				var ptr = Array.GetUnsafePtr();
				var span = new Span<int>(ptr, Array.Length);
				Result.Value = SumSpan(span);
			}

			int SumSpan(Span<int> span)
			{
				var result = 0;

				for(int i = 0; i < span.Length; i++)
				{
					result += span[i];
				}

				return result;
			}
		}

		[Test]
		public static void SpanJobWorks()
		{
			var array = new NativeArray<int>(10, Allocator.TempJob);
			for(int i = 0; i < array.Length; i++)
			{
				array[i] = 1;
			}

			var result = new NativeReference<int>(Allocator.TempJob);

			var job = new SpanJob
			{
				Array = array,
				Result = result
			};
			
			job.Schedule().Complete();
			
			Assert.AreEqual(10, job.Result.Value);

			array.Dispose();
			result.Dispose();
		}
		
	}
}