using DotsLibrary.Collections;
using NUnit.Framework;
using Unity.Collections;
using Unity.Mathematics;
using Unity.PerformanceTesting;
using Random = Unity.Mathematics.Random;

namespace DotsLibrary
{
	public static unsafe class SIMDMathTests
	{
		[Test]
		public static void EnsureSIMDVectorSumWorks()
		{
			// I'm too lazy to not put random values for this
			var count = 100;
			var vectorsA = new float4[count];
			var vectorsB = new float4[count];
			var matrices = new float4x4[count];
			var random = new Random(1);

			for(int i = 0; i < count; i++)
			{
				vectorsA[i] = vectorsB[i] = random.NextFloat4();
				matrices[i] = float4x4.TRS(random.NextFloat3(), quaternion.identity, random.NextFloat3());
			}

			fixed(float4x4* matrixPtr = matrices)
			fixed(float4* vectorPtr = vectorsA)
			{
				SIMDMath.MatrixVectorMultiplySIMD(matrixPtr, vectorPtr, count);
			}

			for(int i = 0; i < count; i++)
			{
				vectorsB[i] = math.mul(matrices[i], vectorsB[i]);
			}

			for(int i = 0; i < count; i++)
			{
				Assert.IsTrue(AlmostEqual(vectorsA[i], vectorsB[i]), $"Failed at index {i}");
			}
		}

		[Test, Performance]
		public static void MatrixMultiplyDefaultBenchmark()
		{
			var count = 100_000;
			NativeArray<float4> vectors = default;
			NativeArray<float4x4> matrices = default;

			Measure.Method(() =>
			       {
				       SIMDMath.MatrixVectorMultiplyDefault(matrices.GetTypedPtr(), vectors.GetTypedPtr(),
					       count);
			       })
			       .SetUp(() =>
			       {
				       vectors = new NativeArray<float4>(count, Allocator.Temp);
				       matrices = new NativeArray<float4x4>(count, Allocator.Temp);
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void MatrixMultiplySIMDBenchmark()
		{
			var count = 100_000;
			NativeArray<float4> vectors = default;
			NativeArray<float4x4> matrices = default;

			Measure.Method(() =>
			       {
				       SIMDMath.MatrixVectorMultiplySIMD(matrices.GetTypedPtr(), vectors.GetTypedPtr(),
					       count);
			       })
			       .SetUp(() =>
			       {
				       vectors = new NativeArray<float4>(count, Allocator.Temp);
				       matrices = new NativeArray<float4x4>(count, Allocator.Temp);
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

		public static bool AlmostEqual(float4 a, float4 b)
		{
			return math.all(math.abs(a - b) < 0.001f);
		}

		[Test]
		public static void RunningSumWorks()
		{
			var count = 1000;
			var intsA = new int[count];
			var intsB = new int[count];
			var random = new Random(1);
			
			for(int i = 0; i < count; i++)
			{
				intsA[i] = intsB[i] = random.NextInt(0, 100);
			}

			fixed(int* ptr = intsA)
			{
				SIMDMath.RunningSumDefault(ptr, count);
			}
			fixed(int* ptr = intsB)
			{
				SIMDMath.RunningSumSIMD(ptr, count);
			}

			for(int i = 0; i < count; i++)
			{
				Assert.AreEqual(intsA[i], intsB[i], $"Failed at index {i}");
			}
		}
		
		[Test, Performance]
		public static void RunningSumSIMDBenchmark()
		{
			var count = 100_000;
			NativeArray<int> ints = default;

			Measure.Method(() =>
			       {
				       SIMDMath.RunningSumSIMD(ints.GetTypedPtr(), count);
			       })
			       .SetUp(() =>
			       {
				       ints = new NativeArray<int>(count, Allocator.Temp);
				       var random = new Random(1);
				       
				       for(int i = 0; i < count; i++)
				       {
					       ints[i] = random.NextInt(0, 100);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void RunningSumDefaultBenchmark()
		{
			var count = 100_000;
			NativeArray<int> ints = default;

			Measure.Method(() =>
			       {
				       SIMDMath.RunningSumDefault(ints.GetTypedPtr(), count);
			       })
			       .SetUp(() =>
			       {
				       ints = new NativeArray<int>(count, Allocator.Temp);
				       var random = new Random(1);
				       
				       for(int i = 0; i < count; i++)
				       {
					       ints[i] = random.NextInt(0, 100);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
	}
}