using NUnit.Framework;
using Unity.Mathematics;
using UnityEngine;

namespace DotsLibrary
{
	public static unsafe class SphereCollisionTests
	{
		[Test]
		public static void SphereCollisionSIMDWorks()
		{
			var sphereCount = 4;
			var sphereXs = new float[sphereCount];
			var sphereYs = new float[sphereCount];
			var sphereZs = new float[sphereCount];
			var sphereRadii = new float[sphereCount];

			sphereXs[0] = 0.5f;
			sphereYs[0] = 0.5f;
			sphereZs[0] = 0.5f;
			sphereRadii[0] = 0.25f;
			
			sphereXs[1] = 0.5f;
			sphereYs[1] = 0.5f;
			sphereZs[1] = 0.5f;
			sphereRadii[1] = 0.25f;

			sphereXs[2] = 0f;
			sphereYs[2] = 0f;
			sphereZs[2] = 0f;
			sphereRadii[2] = 0.1f;
			
			sphereXs[3] = 0f;
			sphereYs[3] = 0f;
			sphereZs[3] = 0f;
			sphereRadii[3] = 0.1f;

			var testSphereArr = new Sphere[1];
			testSphereArr[0] = new Sphere
			{
				Position = new float3(0f, 0f, 0f),
				Radius = 0.2f
			};

			fixed(float* sphereRadiiPtr = sphereRadii)
			fixed(float* sphereZsPtr = sphereZs)
			fixed(float* sphereYsPtr = sphereYs)
			fixed(float* sphereXsPtr = sphereXs)
			fixed(Sphere* testSpherePtr = testSphereArr)
			{
				SphereCollisions.SphereCollisionSIMD_v128(sphereXsPtr, sphereYsPtr, sphereZsPtr, sphereRadiiPtr,
					testSpherePtr, sphereCount, out var intersections);

				Assert.AreEqual(2, intersections);
				
				// TODO: Implement this later!
				// Assert.AreEqual(2, firstIntersectionIndex);
			}
		}

		[Test]
		public static void SphereCollisionNoBranchWorks()
		{
			var sphereCount = 4;
			var sphere = new Sphere[sphereCount];

			// This shouldn't collide
			sphere[0] = new Sphere
			{
				Position = new float3(0.5f, 0.5f, 0.5f),
				Radius = 0.25f,
			};
			sphere[1] = new Sphere
			{
				Position = new float3(0.5f, 0.5f, 0.5f),
				Radius = 0.25f,
			};
			// This collides
			sphere[2] = new Sphere
			{
				Position = float3.zero,
				Radius = 0.1f,
			};
			sphere[3] = new Sphere
			{
				Position = float3.zero,
				Radius = 0.1f,
			};

			var testSphereArr = new Sphere[1];

			testSphereArr[0] = new Sphere
			{
				Position = new float3(0f, 0f, 0f),
				Radius = 0.2f
			};

			fixed(Sphere* spheresPtr = sphere)
			fixed(Sphere* testSpherePtr = testSphereArr)
			{
				SphereCollisions.SphereCollisionNoBranch(testSpherePtr, spheresPtr, sphereCount, out var intersections);

				Assert.AreEqual(2, intersections);
			}
		}

		[Test]
		public static void SphereCollisionDefaultWorks()
		{
			var sphereCount = 4;
			var sphere = new Sphere[sphereCount];

			// This shouldn't collide
			sphere[0] = new Sphere
			{
				Position = new float3(0.5f, 0.5f, 0.5f),
				Radius = 0.25f,
			};
			sphere[1] = new Sphere
			{
				Position = new float3(0.5f, 0.5f, 0.5f),
				Radius = 0.25f,
			};
			// This collides
			sphere[2] = new Sphere
			{
				Position = float3.zero,
				Radius = 0.1f,
			};
			sphere[3] = new Sphere
			{
				Position = float3.zero,
				Radius = 0.1f,
			};

			var testSphereArr = new Sphere[1];

			testSphereArr[0] = new Sphere
			{
				Position = new float3(0f, 0f, 0f),
				Radius = 0.2f
			};

			fixed(Sphere* spheresPtr = sphere)
			fixed(Sphere* testSpherePtr = testSphereArr)
			{
				SphereCollisions.SphereCollisionDefault(testSpherePtr, spheresPtr, sphereCount, out var intersections);

				Assert.AreEqual(2, intersections);
			}
		}
	}
}