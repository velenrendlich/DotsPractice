using NUnit.Framework;
using Unity.Burst;
using Unity.Burst.Intrinsics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.PerformanceTesting;
using UnityEngine;
using Random = Unity.Mathematics.Random;
using static Unity.Burst.Intrinsics.X86.Sse;
using static Unity.Burst.Intrinsics.X86.Sse2;

namespace DotsLibrary
{
	[BurstCompile]
	public static unsafe class FasterMemCmp
	{
		const int Count = 512_000;

		/// <summary>
		/// Comparing the same memory, just checking how long it takes.
		/// </summary>
		[Test, Performance]
		public static void UnityMemCmpBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = UnsafeUtility.MemCmp(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void DefaultMemCmpBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = DefaultMemCmp(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstMemCmpBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = BurstMemCmp(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstMemCmpNoAliasBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = BurstMemCmpNoAlias(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstMemCmpNoAliasUnrolledBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = BurstMemCmpNoAliasUnrolled(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstMemCmpNoAliasCompareAsLongBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = BurstMemCmpNoAliasCompareAsLong(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstMemCmpAsLongPrefetchBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = BurstMemCmpCompareAsLongPrefetch(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstMemCmpSIMDBenchmark()
		{
			NativeArray<byte> bytes = default;

			Measure.Method(() =>
			       {
				       var ptr = bytes.GetUnsafePtr();
				       var areEqual = BurstMemCmpSIMD(ptr, ptr, Count);
			       })
			       .SetUp(() =>
			       {
				       bytes = new NativeArray<byte>(Count, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
				       var random = new Random(1);

				       for(int i = 0; i < Count; i++)
				       {
					       bytes[i] = (byte)random.NextInt(0, 255);
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		/// (0,0): Burst error BC1090: The static constructor `System.Numerics.Vector`1<ushort>..cctor()` is being used by another static constructor in a circular initialization. This is not supported.
		// [BurstCompile]
		// static int BurstMemCmpSpan(void* ptrA, void* ptrB, int count)
		// {
		// 	var spanA = new Span<byte>(ptrA, count);
		// 	var spanB = new Span<byte>(ptrB, count);
		// 	return spanA.SequenceCompareTo(spanB);
		// }

		[Test]
		public static void CompareWorksNonBurst()
		{
			var mem = UnsafeUtility.Malloc(16, 16, Allocator.Temp);
			var a = load_ps(mem);
			var b = load_ps(mem);
			var cmp = cmpeq_epi8(a, b);
			var mask = movemask_epi8(cmp);
			var result = mask == 0xffff;
			
			Assert.IsTrue(result);
		}
		
		[Test]
		public static void CompareWorksBurst_2()
		{
			var memA = new NativeArray<int>(4, Allocator.Temp);
			var memB = new NativeArray<int>(4, Allocator.Temp);
			memA[3] = -1;
			BurstCompare16Bytes(memA.GetUnsafePtr(), memB.GetUnsafePtr(), out var result);
			
			Assert.IsFalse(result);
		}
		
		[Test]
		public static void CompareWorksBurst()
		{
			NativeArray<int> mem = new NativeArray<int>(4, Allocator.Temp);
			mem[0] = -1;
			mem[1] = 1;
			mem[2] = int.MaxValue;
			mem[3] = int.MinValue;
			BurstCompare16Bytes(mem.GetUnsafePtr(), mem.GetUnsafePtr(), out var result);
			
			Assert.IsTrue(result);
		}

		[BurstCompile]
		static void BurstCompare16Bytes(void* a, void* b, out bool result)
		{
			var a128 = load_ps(a);
			var b128 = load_ps(b);
			var cmp = cmpeq_epi8(a128, b128);
			var mask = movemask_epi8(cmp);
			result = mask == 0xffff;
		}
		
		[BurstCompile]
		static int BurstMemCmpSIMD(void* ptrA, void* ptrB, int count)
		{
			var byteA = (byte*)ptrA;
			var byteB = (byte*)ptrB;
			const int simdLength = 16;
			int i;
			
			for(i = 0; i < count - simdLength; i += simdLength)
			{
				var a128 = load_ps(byteA + i);
				var b128 = load_ps(byteB + i);
				var cmp = cmpeq_epi8(a128, b128);
				var mask = movemask_epi8(cmp);
				if(mask != 0xffff)
				{
					return -1;
				}
			}

			for(; i < count; i++)
			{
				if(*(byteA + i) != *(byteB + i))
					return -1;
			}

			return 0;
		}
		
		[BurstCompile]
		static int BurstMemCmpCompareAsLongPrefetch(void* ptrA, void* ptrB, int count)
		{
			var longA = (long*)ptrA;
			var longB = (long*)ptrB;
			count /= sizeof(long);
			
			for(int i = 0; i < count; i++)
			{
				if(*(longA + i) != *(longB + i))
				{
					return -1;
				}
				
				// Bring some longs ahead to the cache?
				Common.Prefetch(longA + i + 2, Common.ReadWrite.Read);
				Common.Prefetch(longB + i + 2, Common.ReadWrite.Read);
			}

			return 0;
		}
		
		[BurstCompile]
		static int BurstMemCmpNoAliasCompareAsLong([NoAlias] void* ptrA, [NoAlias] void* ptrB, int count)
		{
			var longA = (long*)ptrA;
			var longB = (long*)ptrB;
			count /= sizeof(long);
			
			for(int i = 0; i < count; i++)
			{
				if(*(longA + i) != *(longB + i))
				{
					return -1;
				}
			}

			return 0;
		}

		[BurstCompile]
		static int BurstMemCmpNoAliasUnrolled([NoAlias] void* ptrA, [NoAlias] void* ptrB, int count)
		{
			var byteA = (byte*)ptrA;
			var byteB = (byte*)ptrB;
			
			for(int i = 0; i < count; i += 4)
			{
				if(
					*(byteA + i) != *(byteB + i) ||
					*(byteA + i + 1) != *(byteB + i + 1) || 
					*(byteA + i + 2) != *(byteB + i + 2) || 
					*(byteA + i + 3) != *(byteB + i + 3))
				{
					return -1;
				}
			}

			return 0;
		}
		
		[BurstCompile]
		static int BurstMemCmpNoAlias([NoAlias] void* ptrA, [NoAlias] void* ptrB, int count)
		{
			var byteA = (byte*)ptrA;
			var byteB = (byte*)ptrB;
			
			for(int i = 0; i < count; i++)
			{
				if(*(byteA + i) != *(byteB + i))
				{
					return -1;
				}
			}

			return 0;
		}
		
		[BurstCompile]
		static int BurstMemCmp(void* ptrA, void* ptrB, int count)
		{
			var byteA = (byte*)ptrA;
			var byteB = (byte*)ptrB;
			
			for(int i = 0; i < count; i++)
			{
				if(*(byteA + i) != *(byteB + i))
				{
					return -1;
				}
			}

			return 0;
		}
		
		static int DefaultMemCmp(void* ptrA, void* ptrB, int count)
		{
			var byteA = (byte*)ptrA;
			var byteB = (byte*)ptrB;
			
			for(int i = 0; i < count; i++)
			{
				if(*(byteA + i) != *(byteB + i))
				{
					return -1;
				}
			}

			return 0;
		}
	}
}