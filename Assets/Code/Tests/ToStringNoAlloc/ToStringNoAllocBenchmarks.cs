using System.Buffers;
using NUnit.Framework;
using Unity.PerformanceTesting;
using Random = Unity.Mathematics.Random;

namespace DotsLibrary
{
	public static class ToStringNoAllocBenchmarks
	{
		const uint Seed = 3482394;

		static int[] RandomValues = new int[100_000];

		[Test, Performance]
		public static void DefaultIntToString()
		{
			Random random = new Random(Seed);

			Measure.Method(() =>
			       {
				       for(int i = 0; i < 100_000; i++)
				       {
					       var value = RandomValues[i].ToString();
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < 100_000; i++)
				       {
					       RandomValues[i] = random.NextInt();
				       }
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}

		[Test, Performance]
		public static void CustomIntToString()
		{
			Random random = new Random(Seed);

			Measure.Method(() =>
			       {
				       for(int i = 0; i < 100_000; i++)
				       {
					       var mem = ArrayPool<char>.Shared.Rent(12);
					       ;
					       var value = RandomValues[i].ToStringNoAlloc(mem);
					       ArrayPool<char>.Shared.Return(mem);
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < 100_000; i++)
				       {
					       RandomValues[i] = random.NextInt();
				       }
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstIntToString()
		{
			Random random = new Random(Seed);

			Measure.Method(() =>
			       {
				       for(int i = 0; i < 100_000; i++)
				       {
					       var mem = ArrayPool<char>.Shared.Rent(12);
					       ;
					       var value = RandomValues[i].ToStringNoAllocBurst(mem);
					       ArrayPool<char>.Shared.Return(mem);
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < 100_000; i++)
				       {
					       RandomValues[i] = random.NextInt();
				       }
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
	}
}